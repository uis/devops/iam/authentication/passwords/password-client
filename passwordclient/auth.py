"""
Google authentication

"""
import logging
import urllib.parse

import google.auth
import google.auth.impersonated_credentials as impersonated_credentials
import google.auth.transport.requests
import google.oauth2.id_token
import google.oauth2.service_account as service_account

from .settings import settings

LOG = logging.getLogger(__name__)


class AuthError(RuntimeError):
    """
    Raised if there is an error constructing the session or id token.

    """


def google_credentials():
    """
    Create id token credentials for calling the API. Use with AuthenticatedSession:

        from passwordclient.auth import google_credentials, AuthenticatedSession

        session = AuthorizedSession(google_credentials())
        session.get("passwords/test0001/salt")

    """
    audience = settings.api_audience
    if audience is None:
        LOG.error("No API audience. Try setting the PASSWORDS_API_AUDIENCE variable.")
        raise AuthError("No audience specified")

    if settings.google_service_account_credentials is None:
        LOG.info("Using default Google Application Credentials")
        credentials, _ = google.auth.default()
    else:
        LOG.info(
            "Loading service account credentials from: %s",
            settings.google_service_account_credentials
        )
        credentials = service_account.IDTokenCredentials.from_service_account_file(
            settings.google_service_account_credentials,
            target_audience=audience,
        )

    # Configure impersonation if requested.
    if settings.impersonate_google_service_account is not None:
        LOG.info(
            "Impersonating service account: %s",
            settings.impersonate_google_service_account
        )
        credentials = impersonated_credentials.IDTokenCredentials(
            impersonated_credentials.Credentials(
                source_credentials=credentials,
                target_principal=settings.impersonate_google_service_account,
                target_scopes=[]
            ),
            target_audience=audience, include_email=True
        )

    return credentials


class AuthenticatedSession(google.auth.transport.requests.AuthorizedSession):
    """
    Construct a requests.Session object pre-authenticated for the API and pre-configured with a
    base URL.

    As such one can simply use the session as:

        session = AuthenticatedSession()
        session.get("passwords/abc123/salt")

    """
    def __init__(self, credentials=None, *args, **kwargs):
        super().__init__(
            credentials if credentials is not None else google_credentials(),
            *args, **kwargs
        )

    def request(self, method, url, *args, **kwargs):
        if settings.api_base_url is None:
            LOG.error("API base URL not configured. Try setting PASSWORDS_API_BASE_URL.")
            raise AuthError("API base URL not configured.")
        url = urllib.parse.urljoin(settings.api_base_url, url)
        return super().request(method, url, *args, **kwargs)

# Legacy pw-manager like interface to password client.
"""
Command-line interface matching the legacy interface used by the password manager application.

Usage:
    pw-manager (-h | --help)
    pw-manager --version
    pw-manager [--verbose] [--timeout=TIMEOUT]
        [--credentials=FILE] [--audience=AUD] [--impersonate-service-account=EMAIL]
        __google_id_token <api-url> [<timeout>] [<ssh-command>]
    pw-manager [--verbose] <ssh-user> <ssh-host> <timeout> <ssh-command>

Options:

    -h, --help          Show a brief usage summary.

    --version           Show the CLI tool version. This is either a semver formatted value (e.g.
                        1.2.3) or the git commit hash if this is a static binary packaging of an
                        unreleased version.

    --timeout=TIMEOUT   Abort after TIMEOUT seconds if no response is received. [default: 30]

    --credentials=FILE  For "new style" access, specify the Google service account credentials used
                        to authenticate to the API. This can also be specified via the environment.
                        (See "Environment Variables" below.)

    --audience=AUD      For "new style" access, specify the audience of the id token used to call
                        the API. This can also be specified via the environment. (See "Environment
                        Variables" below.)

    --impersonate-service-account=EMAIL
                        For "new style" access, specify the email address to impersonate.  This can
                        also be specified via the environment. (See "Environment Variables" below.)

    --verbose           Be more verbose in output.

    <ssh-user>          Remote user on SSH host.

    <ssh-host>          Remote host to connect to. If prefixed with "RSA:" then the SSH public key
                        algorithms will downgraded to `ssh-rsa`.

    <timeout>           Timeout for SSH connection. If both --timeout and this option are given,
                        this value takes priority.

    <ssh-command>       SSH command to run. Ignored for "new style" API.

    <api-url>           Base API URL. For example: https://my-api.example.com/.

Exit status:

    0       Success.
    100     Invalid command line options.
    200     Invalid sub-command.
    201     Invalid input options for sub-command.
    202     The *auth* user does not exist. (See below for distinction.)
    203     The password passed to the command is incorrect.
    204     The *non-auth* user does not exist. (See below for distinction.)
    207     The password reset token was incorrect.
    208     The password reset token was correct but has expired.
    300     The specified command is not implemented.

    Any other exit status reflects the error returned by the upstream API.

New style access:

    If <ssh-user> is the special value "__google_id_token" then we make use of the "new-style"
    password management API available over REST.

    For this we need to specify the audience for the OpenID identity token used to authenticate to
    the API. This can be done via the command line or via environment variables. See the
    "Environment variables" section below for configuration options.

Input:

    An additional sub-command and its required arguments are passed via stdin, so that sensitive
    data such as CRSids and passwords are not visible in process listings. The input to stdin is
    expected to be a single line of the form:

        <sub-cmd> [<arg>] [<arg>] ...

    where *each* of the sub-command arguments is expected to be base-64 encoded to simplify the
    passing and parsing of the arguments.

    The following sub-commands are supported:

        get-salt <auth-user>

            For the master client remote system (Raven). This sub-command gets the salt (including
            the hashing algorithm) used to store the authorising user's current password. This is
            used so that we can send passwords in hashed form, rather than having to send them
            unencrypted.

            The return value is expected to be a string of the form $id$salt, as used in
            /etc/shadow.

        check-passwd <user> <passwd>

            For the master client remote system (Raven). This sub-command checks that the specified
            *hashed* password is correct for the specified user.

            The password must have been hashed with the salt returned from get-salt.

        set-passwd <auth-user> [<auth-passwd>] <user> <new-passwd>

            Sets a single password on the remote system. On the master client remote system (Raven)
            this requires an authorisation password in addition to the new password being set. On
            all the other (slave) remote client systems the authorisation password is not required.

            Both <auth-passwd> and <new-passwd> should be pre-hashed. The <auth-password> hash
            should use the salt returned from get-salt for the auth user. The salt for <new-passwd>
            may be chosen by the caller.

        set-token <auth-user> <auth-passwd> <user> <token> <expires>

            For the master client remote system (Raven). This sub-command sets a one-time password
            reset token for the specified user. This requires an authorising user and password in
            addition to the user to modify, the token to set on that user and the timestamp
            identifying when the token expires. The expiry timestamp is a 64-bit integer
            (milliseconds since January 1, 1970, 00:00:00 GMT).

            The <auth-passwd> argument should be pre-hashed and should use the salt returned from
            get-salt for the auth user. The salt for <token> may be chosen by the caller.

        delete-token <auth-user> <auth-passwd> <user>

            For the master client remote system (Raven). This sub-command deletes the one-time
            password reset token for the specified user.  This requires an authorising user and
            password in addition to the user to modify. It should not be an error to call this for
            a user who has no token.

            The <auth-passwd> argument should be pre-hashed and should use the salt returned from
            get-salt for the auth user.

        get-token-salt <user>

            For the master client remote system (Raven). This sub-command gets the salt (including
            the hashing algorithm) used to store the specified user's one-time password reset
            token, if they have one.

            The return value is expected to be a string of the form "$id[$rounds=...]$salt", as
            used in /etc/shadow.

        set-passwd-using-token <user> <token> <new-passwd>

            For the master client remote system (Raven). This sub-command sets a single password
            using a one-time password reset token. If the password is successfully updated, the
            token should be invalidated or deleted so that it cannot be used again.

            The <new-passwd> argument should be pre-hashed. The salt can be chosen by the caller.

        recover-passwd <user> <new-passwd>

            For the master client remote system (Raven). This sub-command sets the specified user's
            password to the specified value. This is used during self-service password recovery.
            The user is assumed to not know their current password, but to have successfully proved
            their identity through their recovery email address or mobile phone number, and by
            answering their security questions.

            The <new-passwd> argument should be pre-hashed. The salt can be chosen by the caller.

        lock-account <auth-user> [<auth-passwd>] <user>

            THIS IS NOT IMPLEMENTED FOR "NEW STYLE" ACCESS.

            Locks a user's account on the remote system. On the master client remote system (Raven)
            this requires an authorisation password. On all the other (slave) remote client systems
            the authorisation password is not required.

            The <auth-passwd> argument should be pre-hashed and should use the salt returned from
            get-salt for the auth user.

        unlock-account <auth-user> [<auth-passwd>] <user>

            THIS IS NOT IMPLEMENTED FOR "NEW STYLE" ACCESS.

            Unlocks a user's account on the remote system. On the master client remote system
            (Raven) this requires an authorisation password. On all the other (slave) remote client
            systems the authorisation password is not required.

            The <auth-passwd> argument should be pre-hashed and should use the salt returned from
            get-salt for the auth user.

Environment variables:

    The following environment variables can be set to influence application behaviour. If a
    "passwords.env" file is present in the working directory, the variables will be loaded from
    there as well.

    GOOGLE_APPLICATION_CREDENTIALS

        Path to a Google service account JSON private key file. If empty, the Application Default
        Credentials are used.

    PASSWORDS_IMPERSONATE_GOOGLE_SERVICE_ACCOUNT

        Email address of Google service account to impersonate. If not set, no impersonation is
        performed.

    PASSWORDS_API_AUDIENCE

        Audience for API identity token. Required.

    PASSWORDS_DEBUG

        Set to "1" to enable logging of tracebacks. If unset, tracebacks are suppressed.

"""
import base64
import binascii
import contextlib
import importlib.metadata
import inspect
import logging
import sys
import traceback

import docopt
import paramiko.client

from . import commands
from ..settings import settings

__version__ = importlib.metadata.version("passwordclient")

LOG = logging.getLogger()

COMMANDS = {
    "get-salt": commands.get_salt,
    "check-passwd": commands.check_password,
    "set-passwd": commands.set_password,
    "set-token": commands.set_token,
    "delete-token": commands.delete_token,
    "get-token-salt": commands.get_token_salt,
    "set-passwd-using-token": commands.set_password_using_token,
    "recover-passwd": commands.recover_password,
    "lock-account": commands.lock_account,
    "unlock-account": commands.unlock_account,
}


class SubCommandError(Exception):
    """
    Exception thrown by legacy_command() if the sub command is invalid. The *message* attribute
    should be set to an informative message to show the user.

    """
    def __init__(self, message, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.message = message


def legacy_command(command, args):
    """
    Entry function for all legacy commands.

    """
    # Get callable for command.
    try:
        command_callable = COMMANDS[command]
    except KeyError:
        print(f"No such command: {command}", file=sys.stderr)
        sys.exit(commands.INVALID_COMMAND)

    # Check the number of command line arguments matches the signature.  Usually it is more
    # Pythonic to "ask for forgiveness than permission" which in this case would be catching a
    # hypothetical "wrong number of arguments" exception. Unfortunately, Python overloads the more
    # generic TypeError for this purpose. Even if we found a robust way to detect incorrect
    # argument counts, we'd also need to filter out bugs in the command implementation from us
    # simply calling it wrong. The following is not correct in 100% of cases either (notably
    # variadic functions) but it suffices for our needs.
    expected_args = list(inspect.signature(command_callable).parameters.keys())
    if len(args) != len(expected_args):
        LOG.error(
            "Command %s called with %d argument(s). Expected: %s", command, len(args),
            ', '.join(expected_args)
        )
        sys.exit(commands.INVALID_OPTIONS)

    try:
        # Call command.
        sys.stdout.write(command_callable(*args))
    except commands.CommandError as e:
        # If there was a custom exit status for the command, return it.
        print(e.message, file=sys.stderr)
        sys.exit(e.exit_status)


def dispatch_legacy_command(sub_command_line):
    """
    Parse the sub command and execute it. Throws SubCommandError if the sub command cannot be
    parsed or is unknown.

    """
    sub_command_parts = sub_command_line.split(b' ')
    if len(sub_command_parts) < 1 or sub_command_parts == [b'']:
        raise SubCommandError("no sub command passed on input")

    try:
        sub_command = sub_command_parts[0].decode('ascii')
    except UnicodeDecodeError:
        raise SubCommandError("sub-command must be ASCII")

    try:
        # We _assume_ that the arguments are actually ASCII encoded here. For usernames, passwords,
        # tokens, etc this will be true. In fact, if someone is passing UTF-8 into us they are
        # probably playing silly beggars.
        arguments = [base64.b64decode(a).decode('ascii') for a in sub_command_parts[1:]]
    except binascii.Error:
        raise SubCommandError("sub command arguments could not be parsed as base64")

    # Actually perform the command.
    legacy_command(sub_command, arguments)


def run_command_new_style():
    """
    If we're running the command "new style", read the command from standard input and try to
    execute it.

    """
    try:
        # Read single line from stdin stripping any whitespace.
        sub_command = next(sys.stdin.buffer).strip()
    except StopIteration:
        print("sub-command must be passed on stdin", file=sys.stderr)
        sys.exit(commands.INVALID_COMMAND)

    try:
        # Perform the magic.
        dispatch_legacy_command(sub_command)
    except SubCommandError as e:
        # "Known" errors contain a message for the user which we print and exit with status 200.
        print(e.message, file=sys.stderr)
        sys.exit(commands.INVALID_COMMAND)
    except Exception:
        # "Unknown" errors print a traceback in debug mode but still exit with status 200.
        if settings.debug:
            traceback.print_exc(file=sys.stderr)
        else:
            print("An unexpected error occurred.", file=sys.stderr)
        sys.exit(commands.INVALID_COMMAND)


def run_command_old_style(ssh_user, ssh_host, ssh_command):
    """
    Run the command "old style" via SSH.

    """
    # Create SSH client, load host keys.
    client = paramiko.client.SSHClient()
    client.load_system_host_keys()

    # SSH Hosts prefixed with "RSA:" connect with public key algorithms disabled
    disabled_algorithms = None
    if ssh_host[:4].upper() == 'RSA:':
        ssh_host = ssh_host[4:]
        disabled_algorithms = {'pubkeys': ['rsa-sha2-512', 'rsa-sha2-256']}

    with contextlib.closing(client):
        try:
            # Connect and execute command
            # Public key algorithms need disabling in order to connect to old servers
            client.connect(
                hostname=ssh_host, username=ssh_user, timeout=settings.timeout,
                disabled_algorithms=disabled_algorithms,
            )
        except Exception:
            # Connection errors print a traceback only in debug mode
            if settings.debug:
                traceback.print_exc(file=sys.stderr)
            else:
                print("Remote service connection failure", file=sys.stderr)
            sys.exit(commands.INVALID_COMMAND)

        stdin, stdout, stderr = client.exec_command(ssh_command)

        # Write stdin and wait for exit status from command.
        with contextlib.closing(stdin):
            stdin.write(sys.stdin.buffer.read())
        exit_status = stdout.channel.recv_exit_status()

        # Copy captured stdout and stderr.
        sys.stdout.buffer.write(stdout.read())
        sys.stderr.buffer.write(stderr.read())

    # Teleport exit code from server.
    sys.exit(exit_status)


def main(version=None):
    try:
        # Parse the command line.
        opts = docopt.docopt(
            __doc__ or "", version=version if version is not None else __version__)
        logging.basicConfig(level=logging.INFO if opts["--verbose"] else logging.WARN)

        # Special case: have a timeout.
        if opts["<timeout>"] is not None:
            opts["--timeout"] = opts["<timeout>"]

        # Extract arguments
        try:
            settings.timeout = float(opts["--timeout"])
        except ValueError:
            sys.exit(f"timeout: invalid value: '{opts['--timeout']}'")
    except SystemExit as e:
        # A little bit of Python magic here to adapt docopt's behaviour to the legacy pw-manager
        # script behaviour. If we catch this exception, docopt is attempting to exit. If the code
        # is not 0, exit with a status of 100 indicating an invalid command line.
        if e.code is not None:
            if isinstance(e.code, str):
                # Docopt sets e.code to be a string (which is valid) which we need to print to
                # stderr to mirror the default behaviour.
                print(e.code, file=sys.stderr)
            e.code = 100
        raise e

    if opts["__google_id_token"]:
        # New style API access
        if opts["--impersonate-service-account"] is not None:
            settings.impersonate_google_service_account = opts["--impersonate-service-account"]
        if opts["--credentials"] is not None:
            settings.google_service_account_credentials = opts["--credentials"]
        if opts["--audience"] is not None:
            settings.api_audience = opts["--audience"]

        settings.api_base_url = opts["<api-url>"]
        run_command_new_style()
    else:
        # Old style SSH access
        run_command_old_style(opts["<ssh-user>"], opts["<ssh-host>"], opts["<ssh-command>"])

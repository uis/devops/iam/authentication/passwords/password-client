"""
Commands for legacy CLI.

"""
import datetime
import urllib.parse

from .. import auth
from ..settings import settings

# Exit codes taken from:
# https://gitlab.developers.cam.ac.uk/uis/devops/raven/webauth/-/blob/master/servers/pwd-mgmt/pw-client-new
INVALID_COMMAND = 200
INVALID_OPTIONS = 201
NO_SUCH_AUTH_USER = 202
BAD_PASSWORD = 203
NO_SUCH_USER = 204
BAD_TOKEN = 207
EXPIRED_TOKEN = 208
# Return 200 for unimplemented commands to match previous script. Also exit statuses must be 0-255
NOT_IMPLEMENTED = 200


class CommandError(RuntimeError):
    """
    Exception raised by commands in the case that they are unsuccessful.

    """
    def __init__(self, exit_status, message, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exit_status = exit_status
        self.message = message


def get_salt(username):
    """
    Retrieve the salt for a given user.

    """
    try:
        return f"{_fetch_salt_for_user(username)}\n"
    except UnknownUserError:
        raise CommandError(NO_SUCH_USER, f"No such user: {username}")


def check_password(username, crypted_password):
    """
    Check an existing user's (pre-crypted) password. We may be verifying an inactive user
    so don't exclude them.

    """
    try:
        _verify_crypted_password_for_user(username, crypted_password, exclude_inactive=False)
    except UnknownUserError:
        raise CommandError(NO_SUCH_USER, f"No such user: {username}")
    except IncorrectPasswordError:
        raise CommandError(BAD_PASSWORD, "Password is incorrect")
    return "Password correct\n"


def set_password(auth_user, auth_user_crypted_password, user, new_crypted_password):
    """
    Set a single password after verifying the (possibly different) auth user's password first.

    """
    # if user is setting their own password the we want to not exclude inactive accounts
    _verify_auth_user(auth_user, auth_user_crypted_password, exclude_inactive=(auth_user != user))

    try:
        _set_user_password(user, new_crypted_password)
    except UnknownUserError:
        raise CommandError(NO_SUCH_USER,  f"No such user: {user}")

    return "Password set\n"


def set_token(auth_user, auth_user_crypted_password, user, crypted_token, expiry_timestamp):
    """
    Set a OTP reset token for a user.

    """
    # Only active users can issue tokens so exclude inactive accounts
    _verify_auth_user(auth_user, auth_user_crypted_password, exclude_inactive=True)

    # Convert expiry time which is milliseconds since Jan 1, 1970, 00:00:00 GMT (or UTC) to ISO8601
    # format.
    expiry_str = (
        datetime.timedelta(milliseconds=int(expiry_timestamp)) +
        datetime.datetime(year=1970, month=1, day=1, hour=0, minute=0, second=0)
    ).isoformat()

    # May be setting token for inactive user to don't exclude them
    r = auth.AuthenticatedSession().put(
        f"passwords/{_sanitise_path_component(user)}/reset_token", timeout=settings.timeout,
        json={
            "cryptedToken": crypted_token, "expiresAt": expiry_str,
            "excludeInactive": False,
        }
    )
    if r.status_code == 404:
        raise CommandError(NO_SUCH_USER,  f"No such user: {user}")
    r.raise_for_status()

    return "Token set\n"


def delete_token(auth_user, auth_user_crypted_password, user):
    """
    Delete a user's OTP reset token.

    """
    # Only active users can delete tokens so exclude inactive accounts
    _verify_auth_user(auth_user, auth_user_crypted_password, exclude_inactive=True)

    # May be deleting the token for inactive user to don't exclude them
    r = auth.AuthenticatedSession().delete(
        f"passwords/{_sanitise_path_component(user)}/reset_token", timeout=settings.timeout,
        params={'excludeInactive': False})
    if r.status_code == 404:
        raise CommandError(NO_SUCH_USER,  f"No such user: {user}")
    r.raise_for_status()
    return "Token deleted\n"


def get_token_salt(username):
    """
    Retrieve the token salt for a given user. We may be requesting for an inactive user
    so don't exclude them.

    """
    r = auth.AuthenticatedSession().get(
        f"passwords/{_sanitise_path_component(username)}/reset_token/salt",
        timeout=settings.timeout, params={'excludeInactive': False}
    )
    if r.status_code == 404:
        raise CommandError(NO_SUCH_USER, f"No such user: {username}")
    r.raise_for_status()
    return f"{r.json()['salt']}\n"


def set_password_using_token(username, crypted_token, crypted_password):
    """
    Reset a user's password using the passed reset token for verification.

    """
    try:
        # Firstly, verify the token.
        _verify_crypted_token_for_user(username, crypted_token)

        # If the token is valid, actually set the password.
        _set_user_password(username, crypted_password)
    except UnknownUserError:
        raise CommandError(NO_SUCH_USER,  f"No such user: {username}")
    except IncorrectTokenError:
        raise CommandError(BAD_TOKEN, "Incorrect token")

    return "Password set\n"


def recover_password(username, new_crypted_password):
    """
    Directly set the new crypted password for the passed user without any further verification.

    """
    try:
        _set_user_password(username, new_crypted_password)
    except UnknownUserError:
        raise CommandError(NO_SUCH_USER,  f"No such user: {username}")

    return ""


def lock_account(auth_user, auth_user_crypted_password, user):
    """
    Lock a user account verifying an auth user password first.

    """
    try:
        _verify_crypted_password_for_user(auth_user, auth_user_crypted_password)
    except UnknownUserError:
        raise CommandError(NO_SUCH_AUTH_USER,  f"No such auth user: {user}")
    except IncorrectPasswordError:
        raise CommandError(BAD_PASSWORD, "Incorrect password")

    # Not implemented. This is not implemented by current Raven. See:
    # https://gitlab.developers.cam.ac.uk/uis/devops/raven/webauth/-/blob/master/servers/pwd-mgmt/pw-client-new
    raise CommandError(NOT_IMPLEMENTED, "Command not implemented")


def unlock_account(auth_user, auth_user_crypted_password, user):
    """
    Unlock a user account verifying an auth user password first.

    """
    try:
        _verify_crypted_password_for_user(auth_user, auth_user_crypted_password)
    except UnknownUserError:
        raise CommandError(NO_SUCH_AUTH_USER,  f"No such auth user: {user}")
    except IncorrectPasswordError:
        raise CommandError(BAD_PASSWORD, "Incorrect password")

    # Not implemented. This is not implemented by current Raven. See:
    # https://gitlab.developers.cam.ac.uk/uis/devops/raven/webauth/-/blob/master/servers/pwd-mgmt/pw-client-new
    raise CommandError(NOT_IMPLEMENTED, "Command not implemented")


class IncorrectPasswordError(RuntimeError):
    pass


class IncorrectTokenError(RuntimeError):
    pass


class UnknownUserError(RuntimeError):
    pass


def _sanitise_path_component(v):
    """
    Quote unsafe characters in a patch component into their corresponding percent encoding.

    """
    return urllib.parse.quote(v, safe="")


def _fetch_salt_for_user(username):
    """
    Retrieve the salt for a given user. Raises UnknownUserError if the user does not exist. Raises
    other HTTP exceptions via requests.Response.raise_for_status(). We may be requesting for an
    inactive user so don't exclude them.

    """
    r = auth.AuthenticatedSession().get(
        f"passwords/{_sanitise_path_component(username)}/salt", timeout=settings.timeout,
        params={'excludeInactive': False}
    )
    if r.status_code == 404:
        raise UnknownUserError()
    r.raise_for_status()
    return r.json()['salt']


def _verify_crypted_password_for_user(username, crypted_password, exclude_inactive=True):
    """
    Verify the crypted password for the given user. Raises IncorrectPasswordError if the passwords
    do not match or UnknownUserError if there is no such user. Pass through whether to exclude
    inactive users or not

    """
    r = auth.AuthenticatedSession().post(
        f"passwords/{_sanitise_path_component(username)}/verify", timeout=settings.timeout,
        json={
            "password": crypted_password, "passwordIsCrypted": True,
            "excludeInactive": exclude_inactive,
        }
    )
    if r.status_code == 404:
        raise UnknownUserError()
    r.raise_for_status()
    if not r.json()["isValid"]:
        raise IncorrectPasswordError()


def _verify_crypted_token_for_user(username, crypted_token):
    """
    Verify the crypted reset token for the given user. Raises IncorrectTokenError if the tokens
    do not match or UnknownUserError if there is no such user. We may be verifying the token of
    an inactive user so don't exclude them.

    """
    r = auth.AuthenticatedSession().post(
        f"passwords/{_sanitise_path_component(username)}/reset_token/verify",
        timeout=settings.timeout, json={
            "token": crypted_token, "tokenIsCrypted": True, "excludeInactive": False,
        }
    )
    if r.status_code == 404:
        raise UnknownUserError()
    r.raise_for_status()
    if not r.json()["isValid"]:
        raise IncorrectTokenError()


def _verify_auth_user(auth_user, auth_user_crypted_password, exclude_inactive=True):
    """
    Code common to set_passwd, set_token, etc. to verify an auth user password or raise
    CommandError if is invalid.

    """
    try:
        _verify_crypted_password_for_user(
            auth_user, auth_user_crypted_password, exclude_inactive=exclude_inactive)
    except UnknownUserError:
        raise CommandError(NO_SUCH_AUTH_USER, f"No such user: {auth_user}")
    except IncorrectPasswordError:
        raise CommandError(BAD_PASSWORD, "Auth user password is incorrect")


def _set_user_password(user, new_crypted_password):
    """
    Directly set the new (crypted) password for a user. Raises UnknownUserError if the user does
    not exist. Raises any other HTTP error via requests.Response.raise_for_status().

    Mirroring the behaviour of the original pw-client, setting a password implicitly clears any
    lockout.

    We may be setting the password of an inactive user so don't exclude them.

    """
    r = auth.AuthenticatedSession().put(
        f"passwords/{_sanitise_path_component(user)}", timeout=settings.timeout,
        json={
            "cryptedPassword": new_crypted_password, "clearingLockout": True,
            "excludeInactive": False,
        }
    )
    if r.status_code == 404:
        raise UnknownUserError()
    r.raise_for_status()

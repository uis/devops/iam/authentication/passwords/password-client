"""
Settings used to configure the application.

"""
from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    # Credentials file for Google.
    google_service_account_credentials: Optional[str]

    # Email address of service account to impersonate. If None, no impersonation is performed.
    impersonate_google_service_account: Optional[str] = None

    # Audience for API identity token.
    api_audience: Optional[str]

    # Base URL for passwords API.
    api_base_url: Optional[str]

    # Timeout for API calls
    timeout: float = 30

    # Enable debug reporting
    debug: bool = False

    class Config:
        env_prefix = "passwords_"
        env_file = "passwords.env"
        fields = {
            "google_service_account_credentials": {
                "env": "GOOGLE_APPLICATION_CREDENTIALS",
                "case_sensitive": True,
            },
        }


settings = Settings()

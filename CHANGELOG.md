## Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2022-03-09

### Changed
- Include support for cancelled/inactive (Alumni) accounts


## [1.0.2] - 2022-03-03

### Changed
- Unimplemented (locking) commands return 200 matching previous script


## [1.0.1] - 2022-02-28

### Changed
- Optional public key algorithms disabling by prefixing ssh-host with "RSA:"


## [1.0.0] - 2022-02-28

### Changed
- Public key algorithms disabled in paramiko connect to allow connections to old servers
- Suppress trace on connection failures unless in debug mode


## [0.9.0] - 2021-11-21

### Added
- Initial version

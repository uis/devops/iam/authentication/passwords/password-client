import base64
import io
import unittest
import unittest.mock as mock

from passwordclient import legacy
from passwordclient.legacy.commands import CommandError
from passwordclient.settings import Settings


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        # Patch stdin.
        patcher = mock.patch("sys.stdin")
        stdin = patcher.start()
        stdin.buffer = io.BytesIO()
        self.stdin_mock = stdin.buffer
        self.addCleanup(patcher.stop)

        # Patch command line arguments.
        patcher = mock.patch(
            "sys.argv", ["pw-manager", "__google_id_token", "https://example.com/"])
        self.argv_mock = patcher.start()
        self.addCleanup(patcher.stop)

    def set_subcommand(self, *args):
        """
        Set the sub command, encoding the arguments appropriately.

        """
        self.set_stdin(build_sub_command(*args))

    def set_stdin(self, stdin_value):
        """
        Set the result of reading from stdin.

        """
        self.stdin_mock.seek(0)
        self.stdin_mock.write(stdin_value)
        self.stdin_mock.seek(0)


class DocoptTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        # Patch the legacy command executer.
        patcher = mock.patch("passwordclient.legacy.legacy_command")
        self.legacy_command_mock = patcher.start()
        self.addCleanup(patcher.stop)

    def test_help(self):
        """
        Passing "-h" to pw-manager succeeds.

        """
        self.argv_mock[1:1] = ["-h"]
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertIsNone(ar.exception.code)

    def test_bad_command_line(self):
        """
        An error in the command line exits with status 100.

        """
        self.argv_mock[1:1] = ["--this-switch-does-not-exist"]
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 100)

    def test_no_stdin(self):
        """
        Input on stdin is required and the exit status is 200 if it is missing.

        """
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 200)

    def test_no_subcommand(self):
        """
        A sub-command is required and the exit status is 200 if it is missing.

        """
        self.set_stdin(b'\n')
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 200)

    def test_bad_subcommand(self):
        """
        A badly-formatted sub-command sets the exit status to 200.

        """
        self.set_stdin(b'bad-subcommand not-base64-encoding!')
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 200)

    def test_non_ascii_subcommand(self):
        """
        A non-ascii sub-command sets the exit status to 200.

        """
        self.set_stdin('a-\U0001f4a9-command'.encode('utf8'))
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 200)

    def test_default_timeout(self):
        """
        The default timeout is 30 seconds.

        """
        self.set_subcommand("test-command", "test-arg")
        with mock.patch("passwordclient.legacy.settings", Settings()) as settings:
            # This should end up being set to the default.
            settings.timeout = 10
            legacy.main()
        self.assertEqual(settings.timeout, 30)

    def test_timeout_flag(self):
        """
        The timeout can be changed.

        """
        self.argv_mock.extend(["--timeout", "20"])
        self.set_subcommand("test-command", "test-arg")
        with mock.patch("passwordclient.legacy.settings", Settings()) as settings:
            legacy.main()
        self.assertEqual(settings.timeout, 20)

    def test_bad_timeout_flag(self):
        """
        If the timeout is not a number, the exit status is 100.

        """
        self.argv_mock.extend(["--timeout", "not-a-number"])
        self.set_subcommand("test-command", "test-arg")
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 100)

    def test_unknown_exception(self):
        """
        If legacy_command() raises, the exit status is 200.

        """
        self.set_subcommand("test-command", "test-arg")
        self.legacy_command_mock.configure_mock(side_effect=RuntimeError)
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 200)

    def test_unknown_exception_traceback_suppressed(self):
        """
        If legacy_command() raises and we're not debug mode, the traceback is suppressed and the
        exit status is 200.

        """
        self.set_subcommand("test-command", "test-arg")
        self.legacy_command_mock.configure_mock(side_effect=RuntimeError)

        # If we're not in debug mode, make sure we don't print a traceback.
        with (mock.patch("passwordclient.legacy.settings", Settings()) as settings,
                mock.patch("traceback.print_exc") as print_exc):
            settings.debug = False
            with self.assertRaises(SystemExit) as ar:
                legacy.main()
            self.assertEqual(ar.exception.code, 200)
            print_exc.assert_not_called()

    def test_unknown_exception_traceback(self):
        """
        If legacy_command() raises and we're in debug mode, a traceback is printed and the exit
        status is 200.

        """
        self.set_subcommand("test-command", "test-arg")
        self.legacy_command_mock.configure_mock(side_effect=RuntimeError)

        # If we're in debug mode, make sure we print a traceback.
        with (mock.patch("passwordclient.legacy.settings", Settings()) as settings,
                mock.patch("traceback.print_exc") as print_exc):
            settings.debug = True
            with self.assertRaises(SystemExit) as ar:
                legacy.main()
            self.assertEqual(ar.exception.code, 200)
            print_exc.assert_called()

    def test_settings_on_cli(self):
        """
        Some settings can be provided on the CLI.

        """
        self.argv_mock[1:] = [
            "--credentials", "credfile",
            "--audience", "aud",
            "--impersonate-service-account", "foo@example.com",
            "__google_id_token", "https://example.com/test/"
        ]
        self.set_subcommand("test-command", "test-arg")
        with mock.patch("passwordclient.legacy.settings", Settings()) as settings:
            legacy.main()
        self.assertEqual(settings.google_service_account_credentials, "credfile")
        self.assertEqual(settings.api_audience, "aud")
        self.assertEqual(settings.impersonate_google_service_account, "foo@example.com")
        self.assertEqual(settings.api_base_url, "https://example.com/test/")


class SSHTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        # Mock paramiko client
        patcher = mock.patch("paramiko.client.SSHClient")
        self.mock_client = patcher.start().return_value
        self.addCleanup(patcher.stop)

        # Convenience accessors for std{in,out,err} mocks.
        self.mock_client.exec_command.return_value = (
            mock.MagicMock(), mock.MagicMock(), mock.MagicMock()
        )
        self.ssh_stdin, self.ssh_stdout, self.ssh_stderr = (
            self.mock_client.exec_command.return_value)
        self.ssh_stdout.read = mock.MagicMock(return_value=b'')
        self.ssh_stderr.read = mock.MagicMock(return_value=b'')
        self.ssh_stdout.channel.recv_exit_status.return_value = 0

        self.argv_mock[1:] = ["some-user", "some-host", "15", "some-cmd"]
        self.set_subcommand("test-command", "test-arg")

    def test_timeout_pos_arg(self):
        """
        The timeout is set to the positional arg.

        """
        with (mock.patch("passwordclient.legacy.settings", Settings()) as settings,
                self.assertRaises(SystemExit) as ar):
            legacy.main()
        self.assertEqual(settings.timeout, 15)
        self.assertEqual(ar.exception.code, 0)

    def test_connect_called(self):
        """
        Parameters are passed to paramiko's connect().

        """
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 0)
        self.mock_client.connect.assert_called_with(
            hostname="some-host", username="some-user", timeout=15,
            disabled_algorithms=None,
        )
        self.mock_client.exec_command.assert_called_with("some-cmd")

    def test_connect_called_rsa(self):
        """
        Parameters are passed to paramiko's connect() when ssh-host prefixed
        with "RSA:"

        """
        self.argv_mock[2] = 'RSA:rsa-host'
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 0)
        self.mock_client.connect.assert_called_with(
            hostname="rsa-host", username="some-user", timeout=15,
            disabled_algorithms={'pubkeys': ['rsa-sha2-512', 'rsa-sha2-256']}
        )
        self.mock_client.exec_command.assert_called_with("some-cmd")

    def test_exit_status(self):
        """
        Exit status from server is returned.

        """
        self.ssh_stdout.channel.recv_exit_status.return_value = 201
        with self.assertRaises(SystemExit) as ar:
            legacy.main()
        self.assertEqual(ar.exception.code, 201)


class SubCommandTestCase(unittest.TestCase):
    def test_unknown_sub_command(self):
        """
        Unknown sub commands exit with status 200.

        """
        with self.assertRaises(SystemExit) as ar:
            legacy.legacy_command("not-actually-a-command", [])
        self.assertEqual(ar.exception.code, 200)

    def test_known_sub_command(self):
        """
        Known commands have arguments passed to callable.

        """
        with mock.patch("passwordclient.legacy.COMMANDS") as commands_mock:
            commands_mock["mock-command"] = mock.MagicMock()
            commands_mock["mock-command"].return_value = ""
            legacy.legacy_command("mock-command", ["foo", "bar"])
            commands_mock["mock-command"].assert_called_with("foo", "bar")

    def test_command_exit_status(self):
        """
        If the command implementation raises CommandError, the exit status is preserved.

        """
        with (self.assertRaises(SystemExit) as ar,
                mock.patch("passwordclient.legacy.COMMANDS") as commands_mock):
            commands_mock["mock-command"] = mock.MagicMock()
            commands_mock["mock-command"].side_effect = CommandError(123, "foo")
            legacy.legacy_command("mock-command", ["foo", "bar"])
        self.assertEqual(ar.exception.code, 123)

    def test_bad_subcommand_count(self):
        """
        If the wrong number of options are passed to a subcommand, an exit status of 201
        (INVALID_OPTIONS) is set.

        """
        with (self.assertRaises(SystemExit) as ar,
                mock.patch("passwordclient.legacy.COMMANDS", {}) as commands_mock):
            def get_salt_mock(user):
                return f"Called with user={user}".encode("ascii")
            commands_mock["get-salt"] = get_salt_mock
            legacy.legacy_command("get-salt", ["foo", "bar"])
        self.assertEqual(ar.exception.code, 201)


def build_sub_command(*args):
    """
    Build input which can be passed on standard input to drive a sub-command.

    """
    encoded_args = [a.encode('ascii') for a in args[:1]]
    for a in args[1:]:
        encoded_args.append(base64.b64encode(a.encode('ascii')))
    return b''.join([b' '.join(encoded_args), b'\n'])

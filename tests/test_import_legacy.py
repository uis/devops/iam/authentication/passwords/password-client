def test_import_legacy():
    """
    Importing the legacy tool succeeds.

    """
    import passwordclient.legacy  # noqa: F401

import unittest
import unittest.mock as mock

from passwordclient.auth import google_credentials, AuthenticatedSession, AuthError
from passwordclient.settings import Settings


class AuthTestCase(unittest.TestCase):
    def setUp(self):
        # Mock settings.
        with mock.patch("os.environ", {}):
            # We have to patch the value in the _auth_ module because it is imported as a top-level
            # global within the module. Make sure that the environment is empty and no local env
            # file is loaded.
            patcher = mock.patch("passwordclient.auth.settings", Settings(_env_file=None))
        self.settings_mock = patcher.start()
        self.settings_mock.api_audience = "my-audience"
        self.settings_mock.api_base_url = "https://example.com/base/"
        self.addCleanup(patcher.stop)

        # Mock default credentials.
        patcher = mock.patch("google.auth.default", return_value=(mock.MagicMock(), None))
        self.default_credentials, _ = patcher.start()()
        self.addCleanup(patcher.stop)

    def test_default_credentials(self):
        """
        Default settings use default credentials.

        """
        c = google_credentials()
        self.assertIs(c, self.default_credentials)

    @mock.patch("google.oauth2.service_account.IDTokenCredentials.from_service_account_file")
    def test_load_credentials(self, from_service_account_file_mock):
        """
        If a credentials file is specified, it is used.

        """
        self.settings_mock.google_service_account_credentials = "xxx"
        c = google_credentials()
        from_service_account_file_mock.assert_called_with(
            self.settings_mock.google_service_account_credentials,
            target_audience=self.settings_mock.api_audience
        )
        self.assertIs(c, from_service_account_file_mock.return_value)

    @mock.patch("google.auth.impersonated_credentials.Credentials")
    @mock.patch("google.auth.impersonated_credentials.IDTokenCredentials")
    def test_impersonate(self, id_token_credentials_mock, credentials_mock):
        """
        If a service account to impersonate is specified, it is used.

        """
        self.settings_mock.impersonate_google_service_account = "foo@example.com"
        c = google_credentials()
        credentials_mock.assert_called_with(
            source_credentials=self.default_credentials,
            target_principal="foo@example.com",
            target_scopes=[]
        )
        id_token_credentials_mock.assert_called_with(
            credentials_mock.return_value,
            target_audience=self.settings_mock.api_audience,
            include_email=True
        )
        self.assertIs(c, id_token_credentials_mock.return_value)

    @mock.patch("passwordclient.auth.google_credentials")
    @mock.patch("google.auth.transport.requests.AuthorizedSession.request")
    def test_authenticated_session(self, auth_session_request_mock, _):
        """
        AuthenticatedSession() adds base URL on to request path.

        """
        AuthenticatedSession().get("foo/bar")
        auth_session_request_mock.assert_called_with(
            "GET", f"{self.settings_mock.api_base_url}foo/bar", allow_redirects=True)

    def test_missing_audience(self):
        """
        If the API audience is missing, AuthError is raised.

        """
        self.settings_mock.api_audience = None
        with self.assertRaises(AuthError):
            google_credentials()

    @mock.patch("passwordclient.auth.google_credentials")
    def test_missing_base_url(self, _):
        """
        If the API base URL is missing, AuthError is raised.

        """
        self.settings_mock.api_base_url = None
        with self.assertRaises(AuthError):
            AuthenticatedSession().get("foo/bar")

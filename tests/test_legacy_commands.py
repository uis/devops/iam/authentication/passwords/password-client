import datetime
import unittest
import unittest.mock as mock

from passwordclient.legacy import commands
from passwordclient.settings import settings


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        patcher = mock.patch("passwordclient.auth.AuthenticatedSession")
        self.auth_session_mock = patcher.start().return_value
        self.addCleanup(patcher.stop)


class GetSaltTestCase(BaseTestCase):
    def test_basic_usage(self):
        """
        Basic usage calls the API.

        """
        self.auth_session_mock.get.return_value.json.return_value = {"salt": "some-salt"}
        rv = commands.get_salt("hello")

        # Did we make the right request?
        self.auth_session_mock.get.assert_called_with(
            "passwords/hello/salt", timeout=settings.timeout, params={'excludeInactive': False})

        # Did we check status?
        r = self.auth_session_mock.get.return_value
        r.raise_for_status.assert_called()

        # Did we return the right thing.
        self.assertEqual(rv, "some-salt\n")

    def test_no_such_user(self):
        """
        If there is no such user, return the appropriate exit code.

        """
        self.auth_session_mock.get.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.get_salt("hello")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)


class CheckPasswordTestCase(BaseTestCase):
    def test_basic_usage(self):
        """
        Basic usage calls the API.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        commands.check_password("hello", "goodbye")

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/hello/verify", timeout=settings.timeout,
            json={"password": "goodbye", "passwordIsCrypted": True, "excludeInactive": False}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_incorrect_password(self):
        """
        Incorrect password raises the correct response.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.check_password("hello", "goodbye")
        self.assertEqual(ar.exception.exit_status, commands.BAD_PASSWORD)

    def test_bad_user(self):
        """
        Missing user raises the correct response.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.check_password("hello", "goodbye")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)


class SetPasswordTestCase(BaseTestCase):
    def test_bad_auth_user_password(self):
        """
        Basic usage verifies the auth user.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_password("a1", "a2", "a3", "a4")
        self.assertEqual(ar.exception.exit_status, commands.BAD_PASSWORD)

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_bad_auth_user(self):
        """
        If the auth user does not exist, NO_SUCH_AUTH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_password("a1", "a2", "a3", "a4")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_AUTH_USER)

    def test_bad_user(self):
        """
        If the target user does not exists, NO_SUCH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_password("a1", "a2", "a3", "a4")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)

    def test_success(self):
        """
        Successful setting returns without raising an exception.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 204
        commands.set_password("a1", "a2", "a3", "a4")

        # Did we make the right requests?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )
        self.auth_session_mock.put.assert_called_with(
            "passwords/a3", timeout=settings.timeout,
            json={"cryptedPassword": "a4", "clearingLockout": True, "excludeInactive": False}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()
        r = self.auth_session_mock.put.return_value
        r.raise_for_status.assert_called()

    def test_setting_for_self(self):
        """
        Successful setting for self (auth user and user are the same) includes inactive users

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 204
        commands.set_password("a1", "a2", "a1", "a4")

        # Did we make the right requests?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": False}
        )
        self.auth_session_mock.put.assert_called_with(
            "passwords/a1", timeout=settings.timeout,
            json={"cryptedPassword": "a4", "clearingLockout": True, "excludeInactive": False}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()
        r = self.auth_session_mock.put.return_value
        r.raise_for_status.assert_called()


class SetTokenTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        # Integer timestamps representing "now" and "in the future". Set the fractional second
        # count to 0 to avoid rounding issues.
        self.now = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0)
        self.now_ts = int(self.now.timestamp() * 1000)
        self.future = datetime.timedelta(days=20) + self.now
        self.future_ts = int(self.future.timestamp() * 1000)

    def test_bad_auth_user_password(self):
        """
        Basic usage verifies the auth user.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_token("a1", "a2", "a3", "a4", self.future_ts)
        self.assertEqual(ar.exception.exit_status, commands.BAD_PASSWORD)

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_bad_auth_user(self):
        """
        If the auth user does not exist, NO_SUCH_AUTH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_token("a1", "a2", "a3", "a4", self.future_ts)
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_AUTH_USER)

    def test_bad_user(self):
        """
        If the target user does not exists, NO_SUCH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_token("a1", "a2", "a3", "a4", self.future_ts)
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)

    def test_success(self):
        """
        Successful setting returns without raising an exception.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 204
        commands.set_token("a1", "a2", "a3", "a4", self.future_ts)

        # Did we make the right requests?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )
        self.auth_session_mock.put.assert_called_with(
            "passwords/a3/reset_token", timeout=settings.timeout,
            json={
                "cryptedToken": "a4", "expiresAt": self.future.isoformat().replace('+00:00', ''),
                "excludeInactive": False,
            }
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()
        r = self.auth_session_mock.put.return_value
        r.raise_for_status.assert_called()


class DeleteTokenTestCase(BaseTestCase):
    def test_bad_auth_user_password(self):
        """
        Basic usage verifies the auth user.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.delete_token("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.BAD_PASSWORD)

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_bad_auth_user(self):
        """
        If the auth user does not exist, NO_SUCH_AUTH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.delete_token("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_AUTH_USER)

    def test_bad_user(self):
        """
        If the target user does not exists, NO_SUCH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.delete.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.delete_token("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)

    def test_success(self):
        """
        Successful deletion returns without raising an exception.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.delete.return_value.status_code = 204
        commands.delete_token("a1", "a2", "a3")

        # Did we make the right requests?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )
        self.auth_session_mock.delete.assert_called_with(
            "passwords/a3/reset_token", timeout=settings.timeout,
            params={'excludeInactive': False})

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()
        r = self.auth_session_mock.delete.return_value
        r.raise_for_status.assert_called()


class GetTokenSaltTestCase(BaseTestCase):
    def test_basic_usage(self):
        """
        Basic usage calls the API.

        """
        self.auth_session_mock.get.return_value.json.return_value = {"salt": "some-salt"}
        rv = commands.get_token_salt("hello")

        # Did we make the right request?
        self.auth_session_mock.get.assert_called_with(
            "passwords/hello/reset_token/salt", timeout=settings.timeout,
            params={'excludeInactive': False})

        # Did we check status?
        r = self.auth_session_mock.get.return_value
        r.raise_for_status.assert_called()

        # Did we return the right thing.
        self.assertEqual(rv, "some-salt\n")

    def test_no_such_user(self):
        """
        If there is no such user, return the appropriate exit code.

        """
        self.auth_session_mock.get.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.get_token_salt("hello")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)


class SetPasswordWithTokenTestCase(BaseTestCase):
    def test_bad_token(self):
        """
        Basic usage verifies the token

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_password_using_token("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.BAD_TOKEN)

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/reset_token/verify", timeout=settings.timeout,
            json={"token": "a2", "tokenIsCrypted": True, "excludeInactive": False}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_bad_user(self):
        """
        If the target user does not exists, NO_SUCH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_password_using_token("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)

    def test_user_deletion_race(self):
        """
        If the user somehow gets deleted between us checking the token and then setting the
        password, we behave gracefully.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.set_password_using_token("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)

        # Did we make the right requests?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/reset_token/verify", timeout=settings.timeout,
            json={"token": "a2", "tokenIsCrypted": True, "excludeInactive": False}
        )
        self.auth_session_mock.put.assert_called_with(
            "passwords/a1", timeout=settings.timeout,
            json={"cryptedPassword": "a3", "clearingLockout": True, "excludeInactive": False}
        )

        # Did we check status on the token verification response?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_success(self):
        """
        Successful setting returns without raising an exception.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        self.auth_session_mock.put.return_value.status_code = 204
        commands.set_password_using_token("a1", "a2", "a3")

        # Did we make the right requests?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/reset_token/verify", timeout=settings.timeout,
            json={"token": "a2", "tokenIsCrypted": True, "excludeInactive": False}
        )
        self.auth_session_mock.put.assert_called_with(
            "passwords/a1", timeout=settings.timeout,
            json={"cryptedPassword": "a3", "clearingLockout": True, "excludeInactive": False}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()
        r = self.auth_session_mock.put.return_value
        r.raise_for_status.assert_called()


class RecoverPasswordTestCase(BaseTestCase):
    def test_bad_user(self):
        """
        If the target user does not exists, NO_SUCH_USER is set as the exit status.

        """
        self.auth_session_mock.put.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.recover_password("a1", "a2")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_USER)

    def test_success(self):
        """
        Successful setting returns without raising an exception.

        """
        self.auth_session_mock.put.return_value.status_code = 204
        commands.recover_password("a1", "a2")

        # Did we make the right requests?
        self.auth_session_mock.put.assert_called_with(
            "passwords/a1", timeout=settings.timeout,
            json={"cryptedPassword": "a2", "clearingLockout": True, "excludeInactive": False}
        )

        # Did we check status?
        r = self.auth_session_mock.put.return_value
        r.raise_for_status.assert_called()


class LockAccountTestCase(BaseTestCase):
    def test_bad_auth_user_password(self):
        """
        Basic usage verifies the auth user.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.lock_account("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.BAD_PASSWORD)

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_bad_auth_user(self):
        """
        If the auth user does not exist, NO_SUCH_AUTH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.lock_account("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_AUTH_USER)

    def test_not_implemented(self):
        """
        If auth user credentials are verified, the command raises an exit status indicating it is
        not implemented.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        with self.assertRaises(commands.CommandError) as ar:
            commands.lock_account("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NOT_IMPLEMENTED)

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()


class UnlockAccountTestCase(BaseTestCase):
    def test_bad_auth_user_password(self):
        """
        Basic usage verifies the auth user.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": False}
        with self.assertRaises(commands.CommandError) as ar:
            commands.unlock_account("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.BAD_PASSWORD)

        # Did we make the right request?
        self.auth_session_mock.post.assert_called_with(
            "passwords/a1/verify", timeout=settings.timeout,
            json={"password": "a2", "passwordIsCrypted": True, "excludeInactive": True}
        )

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

    def test_bad_auth_user(self):
        """
        If the auth user does not exist, NO_SUCH_AUTH_USER is set as the exit status.

        """
        self.auth_session_mock.post.return_value.status_code = 404
        with self.assertRaises(commands.CommandError) as ar:
            commands.unlock_account("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NO_SUCH_AUTH_USER)

    def test_not_implemented(self):
        """
        If auth user credentials are verified, the command raises an exit status indicating it is
        not implemented.

        """
        self.auth_session_mock.post.return_value.json.return_value = {"isValid": True}
        with self.assertRaises(commands.CommandError) as ar:
            commands.unlock_account("a1", "a2", "a3")
        self.assertEqual(ar.exception.exit_status, commands.NOT_IMPLEMENTED)

        # Did we check status?
        r = self.auth_session_mock.post.return_value
        r.raise_for_status.assert_called()

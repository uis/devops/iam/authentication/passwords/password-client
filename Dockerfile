# This Dckerfile is intended only to support the Auto-DevOps pipeline on GitLab.
# It's not intended to package the application.
FROM python:3.9-slim

WORKDIR /usr/src/app

# Install specific requirements for the package along with tox to be able to run
# the tests.
ADD requirements.txt ./
RUN pip install tox && pip install -r requirements.txt

# Copy application source and install it.
ADD ./ ./
RUN pip install -e ./

ENTRYPOINT ["pw-manager"]

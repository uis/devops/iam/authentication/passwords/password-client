# Replacement legacy password client for password app

This repository contains a re-implementation of the
[pw-manager](https://gitlab.developers.cam.ac.uk/uis/devops/raven/passwords/passwords/-/blob/master/bin/pw-manager)
script used by the [Raven passwords
app](https://gitlab.developers.cam.ac.uk/uis/devops/raven/passwords/passwords/)
to call password management APIs.

It is intended as a drop-in replacement with progressive enhancement over to
using a REST-based API rather than an SSH-based one.

## Installation

Clone and install the application via:

```console
git clone git@gitlab.developers.cam.ac.uk:uis/devops/raven/legacy/password-client.git
cd password-client
python3 -m venv venv
source venv/bin/activate
pip install -e .
```

## Usage

The `pw-manager` script installed can be used exactly as the existing client to
call SSH-based APIs. E.g:

```console
echo "get-salt $(echo -n spqr1 | base64)" | pw-manager \
    password-mgr password-management.raven.cam.ac.uk 30 /opt/bin/pw-wrapper
```

One can also call the newer password management APIs by using the special user
"__google_id_token":

```console
echo "get-salt $(echo -n spqr1 | base64)" | pw-manager \
    --timeout=30 \
    --credentials=./service-account-credentials.json \
    --audience=123456-abcdefghijk.apps.googleusercontent.com \
    __google_id_token https://password-api.raven.cam.ac.uk/
```

See the output from `pw-manager --help` for more information.

## Service account impersonation

You can also use service account impersonation to make calls. In this case make
sure that your default application credentials have the required permissions.
See [Google's documentation on
impersonation](https://cloud.google.com/iam/docs/impersonating-service-accounts)
for more.

To impersonate a service account use the `--impersonate-service-account`
argument passing the email address of the service account you wish to
impersonate.

## Configuration via environment variables

In addition to command line flags, one can configure aspects of the API
authentication via environment variables or a `passwords.env` file in the
current working directory.

A [template passwords.env](passwords.example.env) file is included. An example
containing the required configuration for production, staging and test instances
of Raven can be found [in the DevOps 1password valut](https://start.1password.com/open/i?a=D3ATZUD36RDHLDKSVJUQZWGORQ&v=fbgfufvsk4aiof2zocsykzj5bq&i=rycc4gchxps3e7f3p22shdsxqe&h=uis-devops.1password.eu).
